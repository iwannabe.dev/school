class School:
    def __init__(self, name, address):
        self.name = name
        self.address = address

class Person:
    def __init__(self, first_name, last_name, address, date_of_birth):
        self.first_name = first_name
        self.last_name = last_name
        self.address = address
        self.date_of_birth = date_of_birth

class Employee(Person):
    def __init__(self, first_name, last_name, address, date_of_birth, annual_leave_entitlement):
        super().__init__(first_name, last_name, address, date_of_birth)
        self.annual_leave_entitlement = annual_leave_entitlement
    
    def request_annual_leave(self, number_of_days):
        print(f"Your holiday rewuest has been accepted. You have \
{self.annual_leave_entitlement - number_of_days} remaining")

class Principal(Employee):
    def request_additional_funding(self):
        print("Our school receives public fundinding only, which isn't enough \
to provide adequate conditions and quality schooling for our pupils, hence I \
kindly ask you for a volounteer donation.")

class Teacher(Employee):
    def assign_homework(self):
        print("Please read chapter 10 and do all excercises on page 27")

class Secretary(Employee):
    def arrange_appointment(self, who, when):
        print(f"I just set up a meeting with {who} on {when}")

    def brew_hot_breverage(self, hot_breverage_type):
        print(f"Here's your {hot_breverage_type} Sir/Madam.")

class SupportStaff(Employee):
    def welcome_guests(self):
        print("Good morning/afternoon Sir/Madam")

class Pupil(Person):
    def do_homework(self):
        print("I'm doing my homework. Please don't bother me now")

    def show_homework(self):
        print("Here's my homework")

class Classroom:
    def __init__(self, classroom_id, classroom_capacity):
        self.classroom_id = classroom_id
        self.classroom_capacity = classroom_capacity

class Subject:
    def __init__(self, subject_name, teacher_object, classroom_object):
        self.subject_name = subject_name


dyro = Principal("Alojzy", "Tromba", "Kalisz", "12/03/1970", 28)
dyro.request_additional_funding()

wuefista = Teacher("Mariusz", "Pudzianowski", "Kalisz", "15/08/1976", 28)
wuefista.request_annual_leave(10)

sekretarka = Secretary("Anna", "Chrzan", "Kalisz", "15/01/1982", 28)
sekretarka.arrange_appointment("Mayor", "Monday")
sekretarka.brew_hot_breverage("tea")

gienek = SupportStaff("Eugeniusz", "Ptak", "Kalisz", "04/08/1966", 28)
gienek.welcome_guests()

jasiu = Pupil("Jan", "Nowak", "Kalisz", "04/18/2014")
jasiu.do_homework()
jasiu.show_homework()

klasa112 = Classroom(112, 25)
wuef = Subject("Wychowanie Fizyczne", wuefista, klasa112)
print(wuef.subject_name)
